package com.Mani.Utilities;

import cucumber.api.Scenario;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;

public class Screenshot {

    public static void takeScreenshot(Scenario scenario, WebDriver browser) {
        //scenario.write("browser is ... : " + browser);
        //scenario.write("scenario is ... : " + scenario);
        byte[] screenshot = ((TakesScreenshot) browser).getScreenshotAs(OutputType.BYTES);
        scenario.embed(screenshot, "image/png");
    }

    public static void captureScreenshot(WebDriver browser) {
        try {
            File src = ((TakesScreenshot) browser).getScreenshotAs(OutputType.FILE);
            String destLocation = "./target/Screenshots/" + System.currentTimeMillis() + ".png";
            FileUtils.copyFile(src, new File(destLocation));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
