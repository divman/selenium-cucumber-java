package com.Mani.Utilities;

import com.Mani.Steps.BaseSteps;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

import static com.Mani.Utilities.Screenshot.*;

public class ServiceHooks {

    @Before
    public static void setupTest(Scenario scenario) {
        BaseSteps.selectBrowser(scenario);
    }

    @After
    public static void tearDown(Scenario scenario) {
        if (scenario.isFailed()) {
            //System.out.println("Scenario Status : " + scenario.getStatus());
            takeScreenshot(BaseSteps.scenario, BaseSteps.browser);
        }
        BaseSteps.browser.quit();
    }
}
