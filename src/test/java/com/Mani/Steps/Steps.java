package com.Mani.Steps;

import com.Mani.Pages.Google_HomePage;
import com.Mani.Utilities.PropertyFileReader;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.log4j.Logger;

import java.util.Properties;

public class Steps extends BaseSteps {

    private Google_HomePage googleHomePage;

    private Logger log = Logger.getLogger(Steps.class);
    private PropertyFileReader propObj = new PropertyFileReader();

    public Steps() {
        googleHomePage = new Google_HomePage(browser);
    }

    @Given("^Open the firefox browser and start google application$")
    public void Open_the_firefox_browser_and_start_google_application() throws Throwable {
        Properties props = propObj.getProperty();
        log.info("Browser opened");
        browser.get(props.getProperty("baseURL"));
        log.info("Google website launched");
    }

    @When("^I enter \"([^\"]*)\" in the seach box and click search button$")
    public void I_enter_in_the_seach_box_and_click_search_button(String SearchText) throws Throwable {
        googleHomePage.enterSearchText(SearchText);
        log.info("Search text typed");
        googleHomePage.clickSearchButton();
        Thread.sleep(2000);
        log.info("Clicked on search button");
    }

    @Then("^I should see the results containing the string:$")
    public void I_should_see_the_results_containing_the_string(String ExpText) throws Throwable {
        googleHomePage.compareResultText(ExpText);
        log.info("Validated the text in the website and success");
    }
}
