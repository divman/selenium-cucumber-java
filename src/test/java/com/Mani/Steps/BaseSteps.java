package com.Mani.Steps;

import cucumber.api.Scenario;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BaseSteps {

    public static WebDriver browser;
    public static Scenario scenario;

    public static void selectBrowser(Scenario scenario) {

        BaseSteps.scenario = scenario;
        Properties systemProperties;

        String remoteURL = ""; // provide the sauce remote url
        String sauceConnectTunnel = ""; // provide any connect tunneling secret
        String browser_name = System.getProperty("browserName");
        String version = System.getProperty("version");
        String os = System.getProperty("os");
        String execPlatform = System.getProperty("execPlatform");
        String username = browser_name + "_" + version + "_" + os;
        DesiredCapabilities dc = new DesiredCapabilities();

        try {
            if (execPlatform.equals("native")) {
                switch (browser_name) {
                    case "ie":
                        browser = new InternetExplorerDriver();
                        browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
                        break;
                    case "chrome":
                        // System.setProperty("webdriver.chrome.driver", "src/test/resources/Drivers/chromedriver");
                        // browser = new ChromeDriver();
                        //	ChromeOptions options = new ChromeOptions();
                        //	options.addArguments("--disable-extensions");
                        //	browser = new ChromeDriver(options);
                        WebDriverManager.chromedriver().setup();
                        browser = new ChromeDriver();
                        browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
                        break;
                    case "firefox":
                        System.setProperty("webdriver.gecko.driver", "src/test/resources/Drivers/geckodriver");
                        browser = new FirefoxDriver();
                        browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
                        break;
                    case "safari":
                        browser = new SafariDriver();
                        browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
                        break;
                    default:
                        browser = new ChromeDriver();
                        browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
                }
            }
            else if (execPlatform.equals("sauce")) {
                URL sauceURL = new URL(remoteURL);
                dc.setBrowserName(browser_name);
                dc.setVersion(version);
                dc.setCapability("platform", os);
                dc.setCapability("browser", browser);
                dc.setCapability("parent-tunnel", sauceConnectTunnel);
                dc.setCapability("name", username);
                dc.setCapability("max-duration", 2700);
                dc.setCapability("command-timeout", 600);
                systemProperties = System.getProperties();
                systemProperties.setProperty("http.proxyHost", ""); // any proxy host
                systemProperties.setProperty("http.proxyPort", ""); // any proxy port

                switch (browser_name) {
                    case "chrome":
                        ChromeOptions options = new ChromeOptions();
                        options.addArguments("test-type");
                        options.addArguments("--disable-extensions");
                        dc.setCapability(ChromeOptions.CAPABILITY, options);
                        break;
                    case "firefox":
                        FirefoxProfile firefoxProfile = new FirefoxProfile();
                        firefoxProfile.setAcceptUntrustedCertificates(true);
                        firefoxProfile.setPreference("pdfjs.disabled", true);
                        firefoxProfile.setPreference("pdfjs.firstRun", false);
                        firefoxProfile.setPreference("plugin.scan.plid.all", false);
                        firefoxProfile.setPreference("plugins.click_to_play", false);
                        firefoxProfile.setPreference("plugin.default.state", 2);
                        firefoxProfile.setPreference("plugin.state.java", 2);
                        firefoxProfile.setPreference("security.enable_java", true);
                        firefoxProfile.setPreference("plugin.scan.Acrobat", "9.0");
                        firefoxProfile.setPreference("network.automatic-ntlm-auth.allow-proxies", true);
                        firefoxProfile.setPreference("network.proxy.autoconfig_url", "<proxyhosst:proxyport>/proxy.pac"); // provide if necessary
                        firefoxProfile.setPreference("network.proxy.no_proxies_on", "localhost, 127.0.0.1"); // provide if necessary
                        dc.setCapability(FirefoxDriver.PROFILE, firefoxProfile);
                        break;
                    case "ie":
                        dc.setBrowserName("internet explorer");
                        break;
                    case "safari":
                        dc.setBrowserName("safari");
                        break;
                    default:
                        System.out.println("Specified browser capabilities are not set");
                }
                System.out.println("Desired caps for Web Driver" + dc + "...");
                browser = new RemoteWebDriver(sauceURL, dc);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        browser.manage().window().maximize();
    }
}
