package com.Mani.Pages;

import com.Mani.Steps.BaseSteps;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import static com.Mani.Utilities.Screenshot.*;
//import org.testng.Assert;

public class Google_HomePage {
    private WebDriver browser;
    private Logger log = Logger.getLogger(Google_HomePage.class);

    @FindBy(how = How.NAME, using = "q")
    private WebElement googleSearchField;

    //@FindBy(how = How.XPATH, using = "//div[@id='sblsbb']/button[@class='lsb' and @name='btnG']")
    @FindBy(how = How.XPATH, using = ".//*[@id='tsf']")
    private WebElement googleSearchButton;

    //@FindBy(how = How.XPATH, using = "//div[@id='rso']/div[1]/div[1]/div/div[1]/div[2]/div[2]/div/span[1]")
    @FindBy(how = How.XPATH, using = "//*[@id=\"rso\"]/div[1]/div/div[1]/div/div[1]/div/div[1]/div/span[1]/span")
    private WebElement googleFirstResultText;

    public Google_HomePage(WebDriver driver) {
        this.browser = driver;
        PageFactory.initElements(driver, this);
    }

    public void enterSearchText(String searchText) {
        googleSearchField.sendKeys(searchText);
    }

    public void clickSearchButton() {
        //googleSearchButton.click();
        googleSearchField.sendKeys(Keys.ENTER);
    }

    public void compareResultText(String ExpText) {
        String ActText = googleFirstResultText.getText();
        //Assert.assertEquals(ActText, ExpText);

        if (ActText.equals(ExpText)) {
            //log.info("The expected ATDD description in the page is as per the required input.");
            System.out.println("The expected ATDD description in the page is as per the required input.");
            //if necessary, to take screenshot randomly anywhere during the test execution.
            takeScreenshot(BaseSteps.scenario, browser);
        } else {
            log.error("Assertion has been failed, the expected and actual ATDD message is not same.");
            Assert.fail("Validation Failes... \nThe Actual page has : "
                    + ActText
                    + " \nBut the expected is : "
                    + ExpText);
        }
    }

}
