package com.Mani.Runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
        features = "src/test/resources/features",
        glue = {"com.Mani.Steps", "com.Mani.Utilities"},
        //tags = {"@regression"},
        plugin = {"pretty",
                "html:target/site/cucumber-pretty",
                "json:target/cucumber.json"}
)

public class TestRunner extends AbstractTestNGCucumberTests {

}
