Feature: Run the sample ATDD featre with logs and html pretty graphical reports

#mvn clean install
#mvn test -Dcucumber.options="--tags @smoke"
#mvn clean install -DbrowserName="chrome" -DexecPlatform="native"
  @regression
  Scenario: Sample scenario 1
    Given Open the firefox browser and start google application
    When I enter "ATDD" in the seach box and click search button
    Then I should see the results containing the string:
		"""
		Acceptance Test Driven Development (ATDD) is an increasingly popular development method for agile teams. ATDD is closely related to Test Driven Development (TDD), but stands out because of its highly collaborative approach.
		"""

  @regression
  Scenario: Sample scenario 2
    Given Open the firefox browser and start google application
    When I enter "ATDD" in the seach box and click search button
    Then I should see the results containing the string:
		"""
		Acceptance Test Driven Development (ATDD) is a practice in which the whole team collaboratively discusses acceptance criteria, with examples, and then distills them into a set of concrete acceptance tests before development begins.
		"""

#@smoke
#Scenario: Sample scenario 3
#	Given Open the firefox browser and start google application
#	When I enter "ATDD" in the seach box and click search button
#	Then I should see the results containing the string:
#		"""
#		ATDD stands for Acceptance Test Driven Development, it is also less commonly designated as Storytest Driven Development (STDD). It is a technique used to bring customers into the test design process before coding has begun.
#		"""
